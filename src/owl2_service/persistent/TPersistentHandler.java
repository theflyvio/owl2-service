/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_service.persistent;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import owl2_lib.cache.TCache;
import owl2_lib.db.TDB;
import owl2_lib.persistent.IPersistentHandler;
import owl2_lib.persistent.IPersistentRecord;
import owl2_lib.sensor.TPersistentEvent;
import owl2_lib.sensor.TPersistentPeriod;
import owl2_service.TService;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;
import x.etc.lib;

/**
 *
 * @author Usuario
 */
public class TPersistentHandler implements IPersistentHandler {

  final private TService service;
  final private TCache cache;
  final private TDB db;
  final private Thread cacheThread;
  final private Thread dbThread;
  private ArrayList<IPersistentRecord> cacheAdd;
  private ArrayList<IPersistentRecord> cacheWrite;

  public TPersistentHandler(final TService service_){
    service = service_;
    cache = new TCache(service.config.get_db_cache_file());
    cacheAdd = new ArrayList<>();
    cacheWrite = new ArrayList<>();
    db = new TDB();
    cacheThread = new Thread() {
      {
        setDaemon(true);
        setName("cache");
      }

      @Override
      public void run(){
        cacheRun();
      }

    };
    //
    dbThread = new Thread() {
      {
        setDaemon(true);
        setName("db");
      }

      @Override
      public void run(){
        dbRun();
      }

    };
  }

  @Override
  public int get_timezone(){
    return service.config.get_timezone();
  }

  public void configure(final String host, final String port, final String database, final String user, final String password){
    db.configure(host, port, database, user, password);
    checkOpen();
    if(!dbThread.isAlive()){
      dbThread.start();
    }
    if(!cacheThread.isAlive()){
      cacheThread.start();
    }
  }

  public void close(){
    cache.close();
  }

  private void checkOpen(){
    if(!cache.isOpen()){
      cache.open();
    }
  }

  public boolean isWorking(){
    checkOpen();
    return cache.isOpen();
  }

  private void swapLists(){
    synchronized(cacheThread){
      final ArrayList<IPersistentRecord> temp = cacheAdd;
      cacheAdd = cacheWrite;
      cacheWrite = temp;
    }
  }

  @Override
  public void write_cache(final IPersistentRecord e){
    synchronized(cacheThread){
      cacheAdd.add(e);
      cacheThread.notify();
    }
  }

  private void cacheRun(){
    while(true){
      for(final IPersistentRecord e : cacheWrite){
        final TXStreamWriter sw = TXStreamWriter.create();
        try{
          put(sw, e);
        }catch(final Exception ex){
          lib.unhandledException(ex);
        }
        cache.write(sw.getByteArray());
      }
      cacheWrite.clear();
      synchronized(dbThread){
        dbThread.notify();
      }
      synchronized(cacheThread){
        while(cacheAdd.isEmpty()){
          try{
            cacheThread.wait();
          }catch(final InterruptedException ex){
          }
        }
        swapLists();
      }
    }
  }

  private void dbRun(){
    //final TDBSQL sql = new TDBSQL(db);
    try{
      //sql.setSQL("insert into gw_events (sensor_sn,server_utc,server_local,event,before_utc,before_local,before_int,after_utc,after_local,after_int,valid,processed) values (?,?,?,?,?,?,?,?,?,?,?,'N')");
      while(true){
        byte[] data;
        synchronized(dbThread){
          while((data = cache.read())==null){
            // cache vazio.
            // deixa o que ja estiver no cache e aguarda o proximo notify ou timeout pra tentar novamente
            try{
              dbThread.wait(60000);
            }catch(final InterruptedException ex){
            }
          }
        }
        final TXStreamReader sr = TXStreamReader.create(data);
        try{
          final IPersistentRecord e = get(sr);
          synchronized(dbThread){
            while(!e.checkStatement()){
              // algo errado com a conexao
              // o proximo notify ou 60s pra tentar novamente
              try{
                dbThread.wait(60000);
              }catch(final InterruptedException ex){
              }
            }
          }
          boolean confirm = e.execute();
          if(!confirm){
            // tenta uma segunda vez devido a desconexoes por timeout
            confirm = e.execute();
            if(!confirm){
              System.err.println("erro ao inserir no banco\r"+e.getDescription());
            }
          }
          if(confirm){
            cache.confirm();
          }
        }catch(final Exception ex){
          lib.unhandledException(ex);
        }
      }
    }catch(final Exception ex){
      lib.unhandledException(ex);
    }
  }

  private void put(final TXStreamWriter sw, final IPersistentRecord e) throws IOException{
    sw.writeInt(1); // versao
    // 1
    sw.writeByte(e.getKind());
    e.save(sw);
  }

  private IPersistentRecord get(final TXStreamReader sr) throws IOException, SQLException{
    IPersistentRecord r;
    int v = sr.readInt();
    //
    switch(sr.readByte()){
      case (byte)'e':
        r = TPersistentEvent.load(sr);
        break;

      case (byte)'p':
        r = TPersistentPeriod.load(sr);
        break;

      default:
        r = null;
        break;
    }
    if(r!=null){
      r.configure(db);
    }
    return r;
  }

}
