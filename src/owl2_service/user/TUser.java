package owl2_service.user;

import java.io.IOException;
import java.util.HashSet;
import owl2_lib.IPeer;
import owl2_lib.sensor.TPack;
import owl2_service.TService;
import owl2_lib.cmd.ICMD_Handler;
import owl2_lib.cmd.TCMD_TX;
import owl2_lib.cmd.TCMD_ack;
import owl2_lib.cmd.TCMD_c;
import owl2_lib.cmd.TCMD_j;
import owl2_lib.cmd.TCMD_l;
import owl2_lib.cmd.TCMD_r_a;
import owl2_lib.cmd.TCMD_r_p;
import owl2_lib.cmd.TCMD_r_s;
import owl2_lib.cmd.TCMD_u_k;
import owl2_lib.sensor.ILog;
import owl2_lib.sensor.TSensor;
import x.etc.lib;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

public class TUser implements ICMD_Handler {

  final private TService service;
  final private HashSet<Integer> watch_list;
  final private ILog log;
  final private int sn;
  private String name;
  private IPeer peer;
  //

  public TUser(final int sn_, final TService service_, final ILog log_){
    sn = sn_;
    service = service_;
    log = log_;
    watch_list = new HashSet<>();
  }

  @Override
  public int get_sn(){
    return sn;
  }

  @Override
  public boolean is_remote_control(){
    return false;
  }

  @Override
  public void peer_connected(final IPeer peer_){
    peer_disconnected(peer_);
    print("user conectado");
    peer = peer_;
  }

  @Override
  public void peer_disconnected(final IPeer peer_){
    if(peer==peer_){
      print("user desconectado");
      final IPeer p = peer;
      peer = null;
      p.close();
    }
  }

//  public TSensor get_sensor_by_sn(final int sn){
//    final TSensor s = service.get_sensor_by_sn(sn, true);
//    sensor_list.add(s); // houve comunicacao nessa conexao
//    return s;
//  }
  @Override
  public void execute_pack_ack(final TPack p, final TCMD_ack c){
  }

  @Override
  public void execute_pack_l(final TPack p, final TCMD_l c){
  }

  @Override
  public void execute_pack_r_a(final TPack p, final TCMD_r_a c){
  }

  @Override
  public void execute_pack_r_p(final TPack p, final TCMD_r_p c){
  }

  @Override
  public void execute_pack_r_s(final TPack p, final TCMD_r_s c){
  }

  @Override
  public void execute_pack_u_k(final TPack p, final TCMD_u_k c){
  }

  @Override
  public void execute_pack_peer_done(){
  }

  @Override
  public void execute_pack_c(final TPack p, final TCMD_c c){
    final TXStreamReader s = TXStreamReader.create(c.cmd);
    final String cmd;
    try{
      cmd = s.readShortString();
      switch(cmd){
        case "finish":
          print("finish");
          service.finish();
          break;

        case "sensor_watch":
          sensor_watch(s);
          break;

        default:
          print("cmd ignorado:"+cmd);
          break;
      }
    }catch(final IOException ex){
      peer.close();
    }
  }

  private void sensor_watch(final TXStreamReader s) throws IOException{
    String error = "";
    String ok = "";
    final int q = s.readInt();
    for(int i = 0; i<q; i++){
      final int add_sn = s.readInt();
      final TSensor sensor = service.get_sensor_by_sn(add_sn);
      if(sensor==null){
        if(error.length()==0){
          error = "não existe:";
        }
        error += " "+add_sn;
      }else{
        if(ok.length()==0){
          ok = "adicionado:";
        }
        ok += " "+add_sn;
      }
      watch_list.add(add_sn);
    }
    send_user_message(ok+"\n"+error);
  }

  @Override
  public void execute_pack_j(final TPack p, final TCMD_j c){
    name = c.name;
  }

  private void send(final byte[] buffer){
    if(peer!=null){
      peer.send(buffer);
    }
  }

  @Override
  public void send_ack(final int n){
    final TCMD_TX a = new TCMD_TX();
    try{
      send(a.u_ask_ack(n, sn));
    }catch(final Exception ex){
      print("erro ao enviar confirmacao n."+n+" / "+ex.getMessage());
    }
  }

  @Override
  public void send_ping(){
    final TCMD_TX r = new TCMD_TX();
    try{
      send(r.u_ask_ping(sn));
    }catch(final Exception ex){
      print("send_ping: "+ex.getLocalizedMessage());
    }
  }

  private void send_user_message(final String msg){
    final TCMD_TX a = new TCMD_TX();
    try{
      final TXStreamWriter s = TXStreamWriter.create();
      s.writeShortString("message");
      s.writeString(msg);
      send(a.u_ask_c(0, sn, s.getByteArray()));
      print("send message: "+msg);
    }catch(final Exception ex){
      print("send_user_message: "+ex.getLocalizedMessage());
    }
  }

  private void send_sensor(final TSensor sensor){
    final TCMD_TX a = new TCMD_TX();
    try{
      final TXStreamWriter s = TXStreamWriter.create();
      s.writeShortString("sensor");
      s.writeInt(sensor.sn);
      sensor.save(s);
      send(a.u_ask_c(0, sn, s.getByteArray()));
      print("send sendor "+sensor.sn);
    }catch(final Exception ex){
      print("send_user_message: "+ex.getLocalizedMessage());
    }
  }

  @Override
  public void pack_invalid(final TPack p){
    print("invalid pack / "+p.cmd);
  }

  /**
   *
   * @param sn
   * @return
   */
  @Override
  public boolean is_gprs_main(final int sn){
    return service.is_gprs_main(sn);
  }

  @Override
  public void on_exception(final String msg, final Exception ex){
    lib.unhandledException(msg, ex);
  }

  @Override
  public void print(final String msg){
    log.print(null, "[user "+sn+"] "+msg);
  }

  public void sensor_changed(final TSensor sensor){
    if(watch_list.contains(sensor.sn)){
      send_sensor(sensor);
    }
  }

}
