/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_service.user;

import java.util.HashMap;
import owl2_lib.sensor.TSensor;
import owl2_service.TService;

/**
 *
 */
public class TUserList {

  final private TService service;
  final private HashMap<Integer, TUser> map;

  public TUserList(final TService service_){
    service = service_;
    map = new HashMap<>();
  }

  public TUser get_by_sn(final int sn, final boolean create){
    TUser user = map.get(sn);
    if(user==null&&create){
      user = new TUser(sn, service, service);
      map.put(sn, user);
    }
    return user;
  }

  public void sensor_changed(final TSensor sensor){
    map.forEach((sn, user)-> user.sensor_changed(sensor));
  }
}
