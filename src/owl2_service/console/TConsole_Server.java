/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_service.console;

import owl2_service.TPeer;
import owl2_service.TService;
import owl2_service.TServiceConfig;
import x.etc.io.socket.tcp.TXTCPServer;
import x.etc.timer.TXTimer;

/**
 *
 */
public class TConsole_Server extends TXTCPServer<TPeer> {

  final static private int ACTIVE_TIME_CHECK = 30000;

  final private Object lock;
  final private TService service;
  final private TXTimer timerCheckConnection;

  public TConsole_Server(final TService service_, final TServiceConfig config_){
    super(service_.tcp);
    lock = new Object();
    service = service_;
    timerCheckConnection = new TXTimer(ACTIVE_TIME_CHECK) {
      @Override
      public void run(){
        // a cada 60s verifica quem teve atividade, se nao teve, desconecta
        synchronized(lock){
          for(int i = getPeerCount()-1; i>=0; i--){
            final TPeer peer = getPeerByIndex(i);
            if(peer.activity>0){
              peer.activity--;
            }else{
              peer.close();
            }
          }
        }
      }
    };
    timerCheckConnection.start();
  }

  public boolean reconfigure_and_listen(){
    close();
    setPort(service.config.get_console_port());
    return listen();
  }

  @Override
  protected TPeer newPeer(){
    return new TPeer(service);
  }

  @Override
  protected void onPeerConnected(final TPeer t){
  }

  @Override
  protected void onPeerDisconnected(final TPeer t){
  }

  @Override
  protected void onConnect(){
  }

  @Override
  protected void onCantConnect(){
  }

  @Override
  protected void onDisconnect(){
  }

}
