/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;
import x.etc.io.streams.TXStreamReader;
import x.etc.io.streams.TXStreamWriter;

/**
 *
 */
public class TServiceConfig {

  //
  final static public String GPRS_PORT_KEY = "gprs_port";
  final static public int GPRS_PORT_DEFAULT = 23013;
  //
  final static public String CONSOLE_PORT_KEY = "console_port";
  final static public int CONSOLE_PORT_DEFAULT = 35793;
  //
  final static public String DB_CACHE_FILE_KEY = "db_cache_file";
  final static public String DB_CACHE_FILE_DEFAULT = "./owl2.dbcache";
  //
  final static public String TIMEZONE_KEY = "timezone";
  final static public int TIMEZONE_DEFAULT = -3;
  //
  final static public String DB_HOST_KEY = "db_host";
  final static public String DB_HOST_DEFAULT = "192.168.20.46";
  //
  final static public String DB_PORT_KEY = "db_port";
  final static public int DB_PORT_DEFAULT = 5432;
  //
  final static public String DB_NAME_KEY = "db_name";
  final static public String DB_NAME_DEFAULT = "owl";
  //
  final static public String DB_USER_KEY = "db_user";
  final static public String DB_USER_DEFAULT = "postgres";
  //
  final static public String DB_PASS_KEY = "db_pass";
  final static public String DB_PASS_DEFAULT = "postgres";
  //
  //
  final private String filename;
  final private Properties cfg;

  public TServiceConfig(final String filename_){
    filename = filename_;
    cfg = new Properties();
  }

  private void check_defaults(){
    check_default(GPRS_PORT_KEY, GPRS_PORT_DEFAULT);
    check_default(CONSOLE_PORT_KEY, CONSOLE_PORT_DEFAULT);
    check_default(DB_CACHE_FILE_KEY, DB_CACHE_FILE_DEFAULT);
    check_default(TIMEZONE_KEY, TIMEZONE_DEFAULT);
    check_default(DB_HOST_KEY, DB_HOST_DEFAULT);
    check_default(DB_PORT_KEY, DB_PORT_DEFAULT);
    check_default(DB_NAME_KEY, DB_NAME_DEFAULT);
    check_default(DB_USER_KEY, DB_USER_DEFAULT);
    check_default(DB_PASS_KEY, DB_PASS_DEFAULT);
  }

  public int get_timezone(){
    return get_int(TIMEZONE_KEY, TIMEZONE_DEFAULT);
  }

  public void set_timezone(final int tz){
    set_integer(TIMEZONE_KEY, tz);
  }

  public int get_gprs_port(){
    return get_int(GPRS_PORT_KEY, GPRS_PORT_DEFAULT);
  }

  public void set_gprs_port(final int port){
    set_integer(GPRS_PORT_KEY, port);
  }

  public int get_console_port(){
    return get_int(CONSOLE_PORT_KEY, CONSOLE_PORT_DEFAULT);
  }

  public void set_console_port(final int port){
    set_integer(CONSOLE_PORT_KEY, port);
  }

  public String get_db_cache_file(){
    return get_string(DB_CACHE_FILE_KEY, DB_CACHE_FILE_DEFAULT);
  }

  public void set_db_cache_file(final String filename){
    set_string(DB_CACHE_FILE_KEY, filename);
  }

  public String get_db_host(){
    return get_string(DB_HOST_KEY, DB_HOST_DEFAULT);
  }

  public void set_db_host(final String v){
    set_string(DB_HOST_KEY, v);
  }

  public int get_db_port(){
    return get_int(DB_PORT_KEY, DB_PORT_DEFAULT);
  }

  public void set_db_port(final int v){
    set_integer(DB_PORT_KEY, v);
  }

  public String get_db_name(){
    return get_string(DB_NAME_KEY, DB_NAME_DEFAULT);
  }

  public void set_db_name(final String v){
    set_string(DB_NAME_KEY, v);
  }

  public String get_db_user(){
    return get_string(DB_USER_KEY, DB_USER_DEFAULT);
  }

  public void set_db_user(final String v){
    set_string(DB_USER_KEY, v);
  }

  public String get_db_pass(){
    return get_string(DB_PASS_KEY, DB_PASS_DEFAULT);
  }

  public void set_db_pass(final String v){
    set_string(DB_PASS_KEY, v);
  }

  /*
   *
   *
   *
   *
   *
   *
   *
   *
   *
   */
  public void set_string(final String key, final String value){
    synchronized(cfg){
      cfg.setProperty(key, value);
      save_to_file();
    }
  }

  public void set_integer(final String key, final int value){
    synchronized(cfg){
      cfg.setProperty(key, String.valueOf(value));
      save_to_file();
    }
  }

  public String get_string(final String key, final String default_value){
    synchronized(cfg){
      return cfg.getProperty(key, default_value);
    }
  }

  public int get_int(final String key, final int default_value){
    synchronized(cfg){
      return Integer.parseInt(cfg.getProperty(key, Integer.toString(default_value)));
    }
  }

  private void check_default(final String key, final String value){
    if(!cfg.containsKey(key)){
      cfg.setProperty(key, value);
    }
  }

  private void check_default(final String key, final int value){
    if(!cfg.containsKey(key)){
      cfg.setProperty(key, Integer.toString(value));
    }
  }

  public void save_to_file(){
    synchronized(cfg){
      try (final OutputStream output = new FileOutputStream(filename)){
        // save_to_file properties to project root folder
        check_defaults();
        cfg.store(output, null);

      }catch(final Exception ex){
        ex.printStackTrace();
      }
    }
  }

  public void load_from_file(){
    synchronized(cfg){
      try (final InputStream input = new FileInputStream(filename)){
        cfg.load(input);

      }catch(final FileNotFoundException ex){
        save_to_file();

      }catch(final Exception ex){
        ex.printStackTrace();
      }
    }
  }

  public void print(){
    synchronized(cfg){
      System.out.println("Properties:");
      cfg.forEach((k, v) -> System.out.println(k+"="+v));
    }
  }

  public void save(final TXStreamWriter s) throws IOException{
    synchronized(cfg){
      s.writeInt(1);
      //
      s.writeInt(cfg.size());
      @SuppressWarnings("unchecked")
      final Enumeration<String> enums = (Enumeration<String>)cfg.propertyNames();
      while(enums.hasMoreElements()){
        final String k = enums.nextElement();
        s.writeString(k);
        s.writeString(cfg.getProperty(k));
      }
    }
  }

  public void load(final TXStreamReader s) throws IOException{
    synchronized(cfg){
      s.readInt();
      //
      int i = s.readInt();
      while(i>0){
        cfg.setProperty(s.readString(), s.readString());
      }
    }
  }

}
