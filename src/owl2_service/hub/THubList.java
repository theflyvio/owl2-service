/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_service.hub;

import java.util.HashMap;
import owl2_lib.sensor.ESensorType;
import owl2_lib.sensor.TSensor;
import owl2_service.TService;

/**
 *
 */
public class THubList {

  final private TService service;
  final private HashMap<Integer, THub> map;

  public THubList(final TService service_){
    service = service_;
    map = new HashMap<>();
  }

  public THub get_by_sn(final int sn, final ESensorType type, final boolean create){
    THub hub = map.get(sn);
    if(hub==null&&create){
      final TSensor s = service.get_sensor_by_sn(sn, type, true);
      if(s!=null){
        hub = new THub(service, s, service);
        map.put(sn, hub);
      }
    }
    return hub;
  }
}
