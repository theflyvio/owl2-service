package owl2_service.hub;

import ab.etc.data.TUTC;
import owl2_lib.sensor.TPack;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import owl2_lib.IPeer;
import owl2_lib.sensor.TSensor;
import owl2_lib.sensor.TSensorList;
import owl2_service.TService;
import owl2_lib.cmd.ICMD_Handler;
import owl2_lib.cmd.TCMD_TX;
import owl2_lib.cmd.TCMD_ack;
import owl2_lib.cmd.TCMD_c;
import owl2_lib.cmd.TCMD_j;
import owl2_lib.cmd.TCMD_l;
import owl2_lib.cmd.TCMD_r_a;
import owl2_lib.cmd.TCMD_r_p;
import owl2_lib.cmd.TCMD_r_s;
import owl2_lib.cmd.TCMD_u_k;
import owl2_lib.sensor.ESensorType;
import owl2_lib.sensor.ILog;
import x.etc.lib;
import owl2_lib.update.TFirmware;
import owl2_lib.update.TFirmwareUpdate;
import z_owl.EProgram;

public class THub implements ICMD_Handler {

  final private TService service;
  final private TSensor sensor;
  final private TSensorList sensor_list;
  final private ILog log;
  private boolean remote_control;
  private IPeer peer;
  private ZonedDateTime last_disconnect;
  //
  private boolean rtc_sent;
  private String fw_update_error;
  private TFirmwareUpdate fw_update;

  public THub(final TService service_, final TSensor sensor_, final ILog log_){
    service = service_;
    sensor = sensor_;
    log = log_;
    sensor_list = service.create_sensor_list();
  }

  public void set_remote_control(final boolean rc){
    remote_control = rc;
  }

  @Override
  public boolean is_remote_control(){
    return remote_control;
  }

  public void set_update(final TFirmware fw){
    if(fw.getProgram()==EProgram.MESH_HUB&&sensor.fw_version>=fw.getVersion()){
      fw_update = null;
      fw_update_error = "firmware update hub sensor.version>=fw.version";
      print(fw_update_error);

    }else{
      fw_update = new TFirmwareUpdate(fw);
      fw_update_error = null;
    }
  }

  @Override
  public int get_sn(){
    return sensor.sn;
  }

  @Override
  public void peer_connected(final IPeer peer_){
    peer_disconnected(peer_);
    print("conectado");
    sensor_list.clear();
    sensor_list.add(sensor);
    peer = peer_;
    // reseta a cada conexao
    rtc_sent = false;
  }

  @Override
  public void peer_disconnected(final IPeer peer_){
    if(peer==peer_){
      print("desconectado");
      final IPeer p = peer;
      peer = null;
      p.close();
      last_disconnect = ZonedDateTime.now(ZoneOffset.UTC);
    }
  }

  public TSensor get_sensor(final int sn){
    final TSensor s = service.get_sensor_by_sn(sn, sn==sensor.sn ? ESensorType.GPRS_MAIN : ESensorType.GPRS_SECONDARY, true);
    sensor_list.add(s); // houve comunicacao nessa conexao
    print("obtendo sensor "+sn+" : "+(s==null ? "null" : "ok"));
    return s;
  }

  @Override
  public void execute_pack_ack(final TPack p, final TCMD_ack c){
  }

  @Override
  public void execute_pack_l(final TPack p, final TCMD_l c){
  }

  @Override
  public void execute_pack_c(final TPack p, final TCMD_c c){
  }

  @Override
  public void execute_pack_j(final TPack p, final TCMD_j c){
  }

  @Override
  public void execute_pack_r_a(final TPack p, final TCMD_r_a c){
    final TSensor s = get_sensor(c.header.sn);
    if(s!=null){
      s.execute_cmd(c);
    }else{
      print("pack_r_a registro ignorado, sn nao encontrado "+c.header.sn);
    }
  }

  @Override
  public void execute_pack_r_p(final TPack p, final TCMD_r_p c){
    final TSensor s = get_sensor(c.header.sn);
    if(s!=null){
      s.execute_cmd(c);
    }else{
      print("pack_r_p registro ignorado, sn nao encontrado "+c.header.sn);
    }
  }

  @Override
  public void execute_pack_r_s(final TPack p, final TCMD_r_s c){
    final TSensor s = get_sensor(c.header.sn);
    if(s!=null){
      s.execute_cmd(c);
    }else{
      print("pack_r_s registro ignorado, sn nao encontrado "+c.header.sn);
    }
  }

  @Override
  public void execute_pack_u_k(final TPack p, final TCMD_u_k c){
    if(fw_update!=null){
      if(c.u.version!=fw_update.getVersion()){
        fw_update_error = "pack_u_k wrong version";
        fw_update = null;
        print(fw_update_error);
      }else if(c.u.part_count!=fw_update.getPartCount()){
        fw_update_error = "pack_u_k wrong pack_count";
        fw_update = null;
        print(fw_update_error);
      }else{
        fw_update.setPartSent(c.part);
      }
    }
    update_send_next();
  }

  @Override
  public void execute_pack_peer_done(){
    boolean any_send = false;
    print("peer_done");
    if(!rtc_sent){
      rtc_sent = true;
      send_req_type(null, 'h');
      any_send = true;
    }
    final long now_ms = TUTC.now().toMs();
    for(int i = 0; i<sensor_list.count(); i++){
      final TSensor s = sensor_list.get_by_index(i);
      if(s==null){
        print("peer_done sensor null");
      }else{
        if(!s.is_configured()&&(now_ms-s.last_req_config>120000)){
          if(!s.config.configured){
            s.cmds.set_req_config();
            s.last_req_config = now_ms;
          }
          send_config(s);
          any_send = true;
        }
        if(s.cmds.has_mux_pot()){
          if(s.cmds.get_mux()!=s.install.mux||s.cmds.get_pot()!=s.install.pot){
            send_req_type(s, 'a');
            s.cmds.mux_pot_sent();
            any_send = true;
          }else{
            s.cmds.mux_pot_cancel();
          }
        }
        if(s.cmds.has_req_config()){
          send_req_type(s, 'c');
          s.cmds.req_config_sent();
          any_send = true;
        }
        if(s.cmds.has_clear_flash()){
          send_req_type(s, 'x');
          s.cmds.clear_flash_sent();
          any_send = true;
        }
      }
    }
    //
//    if(req_install_off){
//      send_req_type(null, 'i');
//      req_install_off = false;
//      any_send = true;
//    }
//    if(req_period_size>0){
//      working_period_size = req_period_size;
//      send_req_type(null, 'p');
//      req_period_size = 0;
//      any_send = true;
//    }
//    if(req_connect_spread>0){
//      send_req_type(null, 'o');
//      req_connect_spread = 0;
//      any_send = true;
//    }
    if(!any_send){
      update_send_next();
    }
  }

  private void update_send_next(){
    if(fw_update!=null){
      if(!send_update(fw_update)){
        fw_update = null;
        update_send_next();
      }
    }else if(!remote_control){
      send_server_done();
    }
  }

  public boolean send_update(final TFirmwareUpdate fwu){
    final TCMD_TX r = new TCMD_TX();
    try{
      final byte[] data = r.g_ask_u_p(0, fwu);
      if(data!=null){
        send(data);
        print("sending update "+fwu.getProgram()+" "+fwu.getVersion()+" "+fwu.getPartSent()+"/"+fwu.getPartCount());
        return true;
      }
    }catch(final Exception ex){
      fw_update_error = "error sending update "+ex.getLocalizedMessage();
      print(fw_update_error);
    }
    return false;
  }

  public void send(final byte[] buffer){
    if(peer!=null){
      peer.send(buffer);
    }
  }

  @Override
  public void send_ack(final int n){
    final TCMD_TX a = new TCMD_TX();
    try{
      send(a.g_ask_ack(n));
    }catch(final Exception ex){
      print("erro ao enviar confirmacao n."+n+" / "+ex.getMessage());
    }
  }

  @Override
  public void send_ping(){
    final TCMD_TX r = new TCMD_TX();
    try{
      send(r.g_ask_ping());
    }catch(final Exception ex){
    }
  }

  public void send_req_type(final TSensor s, final char req_type){
    final TCMD_TX r = new TCMD_TX();
    final int sn = s==null ? 0 : s.sn;
    String msg = "";
    try{
      switch(req_type){
        case 'a':
          send(r.g_ask_q_a(0, s.sn, s.cmds.get_mux(), s.cmds.get_pot(), s.cmds.get_mux_pot_samples()));
          msg = "set_mux "+s.cmds.get_mux()+","+s.cmds.get_pot()+","+s.cmds.get_mux_pot_samples();
          break;

        case 'c':
          send(r.g_ask_q_c(0, sn));
          msg = "req_config";
          break;

        case 'i':
          send(r.g_ask_q_i(0, sn));
          msg = "install_off";
          break;

        case 'h':
          send(r.g_ask_q_h(0, sn));
          msg = "rtc";
          break;

        case 'o':
          send(r.g_ask_q_o(0, sn, s.config.connect_spread));
          msg = "spread="+s.config.connect_spread;
          break;

        case 'p':
          send(r.g_ask_q_p(0, sn, s.config.period_size));
          msg = "period_size="+s.config.period_size;
          break;

        case 'x':
          send(r.g_ask_q_x(0, sn));
          msg = "clear_flash";
          break;

      }
    }catch(final Exception ex){
      msg += "error";
    }
    msg = " send cmd \""+req_type+"\" "+msg;
    if(s==null){
      print("broadcast "+msg);
    }else{
      s.print(msg);
    }
  }

  private void send_config(final TSensor s){
    final TCMD_TX b = new TCMD_TX();
    try{
      send(b.g_ask_b(0, s.sn, s.target_config));
      s.print("send_config "+sensor.config.get_print());
      s.config.set(s.target_config);
      if(s.config.set_cag==3){
        s.config.set_cag = 1;
      }
    }catch(final Exception ex){
    }
  }

  @Override
  public void pack_invalid(final TPack p){
    print("invalid pack / "+p.cmd);
  }

  private void send_server_done(){
    print("server_done\n");
    final TCMD_TX r = new TCMD_TX();
    try{
      send(r.g_ask_server_done());
    }catch(final Exception ex){
      lib.unhandledException(ex);
    }
  }

  /**
   *
   * @param sn
   * @return
   */
  @Override
  public boolean is_gprs_main(final int sn){
    return service.is_gprs_main(sn);
  }

  @Override
  public void on_exception(final String msg, final Exception ex){
    lib.unhandledException(msg, ex);
  }

  @Override
  public void print(final String msg){
    log.print(null, "[hub "+sensor.sn+"] "+msg);
  }

//  final private int id;
//  final private ArrayList<TGWSensor> sensorList;
//  //
//  final private TGWSensor sensor;
//  //
//  private int update_cag;
//  private boolean req_config;
//  private boolean req_install_off;
//  private int req_period_size;
//  private int working_period_size;
//  private int req_connect_spread;
//  private boolean req_clear_flash_lists;
//  //
//  private boolean rtc_sent;
//  final private TXTimer timer_remote_control;
//
//  public THub(final TGWGPRS_Section owner_, final int id_){
//    owner = owner_;
//    id = id_;
//    sensorList = new ArrayList<>();
//    callOnChanged = new TXCallControl(() -> owner.onChanged());
//    lastDisconnect = ZonedDateTime.now(ZoneOffset.UTC); //.minusMinutes(15);
//    sensor = owner.getSensorBySN(id, new TCoordinates(0, 0), 0, EGWSensorType.GPRS_MAIN);
//    update_cag = -1;
//    working_period_size = 15; // PARA TESTES
//    timer_remote_control = new TXTimer(30000) {
//      @Override
//      public void run(){
//        send_ping();
//      }
//    };
//  }
//
//  public boolean isConnected(){
//    return peer!=null;
//  }
//
//  public ZonedDateTime getLastDisconnect(){
//    return lastDisconnect;
//  }
//
//  public void save(final TXStreamWriter sw) throws Exception{
//    sw.writeByte((byte)2);
//    //
//    sw.writeZonedDateTime(lastDisconnect);
//  }
//
//  public void load(final TXStreamReader sr) throws Exception{
//    callOnChanged.lock();
//    try{
//      final int v = sr.readByte();
//      if(v>=2){
//        lastDisconnect = sr.readZonedDateTime();
//      }
//      //
//    }finally{
//      callOnChanged.unlock();
//    }
//  }
}
