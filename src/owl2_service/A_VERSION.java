package owl2_service;

/**
 * SERVICE
 */
public class A_VERSION {

  final static public String app = "owl2_service";
  final static public int version = 1;
  final static public int owl2_service_lib_min = 1;
  final static public int owl2_lib_min = 1;
  //

  /*
   * 25/10/2020 1
   * inicial
   */
  static public void check(){
    System.out.println(app+".version="+version);
    owl2_service_lib.A_VERSION.check(app, owl2_service_lib_min);
    owl2_lib.A_VERSION.check(app, owl2_lib_min);
  }

}
