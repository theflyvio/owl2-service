/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_service;

import owl2_service.persistent.TPersistentHandler;
import owl2_lib.sensor.ESensorType;
import owl2_lib.sensor.ILog;
import owl2_lib.sensor.TSensor;
import owl2_lib.sensor.TSensorList;
import owl2_service.console.TConsole_Server;
import owl2_service.gprs.TGPRS_Server;
import owl2_service.hub.THub;
import owl2_service.hub.THubList;
import owl2_service.user.TUser;
import owl2_service.user.TUserList;
import x.etc.io.socket.tcp.TXTCP;

/**
 *
 * esquema para copiar para o server 13:
 *
 * no dell, deixar rodando o bat c:\service_copy.bat
 *
 * no server 13 deixar rodando o bat c:\owl2\\update_check.bat
 *
 * compilar
 * executar E:\dev\Nastek\owl2\owl2-service\\update_to_13.bat
 * esperar terminar a copia para o livre
 *
 * no console:
 * logar, mandar finalizar trefa
 *
 *
 */
public class TService implements ILog {

  final private Object lock;
  //
  final private TSensorList sensor_list;
  final private THubList hub_list;
  final private TUserList user_list;
  final private TPersistentHandler persistent_handler;
  final private TGPRS_Server gprs_server;
  final private TConsole_Server console_server;
  final public TServiceConfig config;
  final public TXTCP tcp;
  //
  private boolean finished;

  public TService(final String config_filename){
    lock = new Object();
    config = new TServiceConfig(config_filename);
    persistent_handler = new TPersistentHandler(this);
    sensor_list = new TSensorList(persistent_handler, this);
    hub_list = new THubList(this);
    user_list = new TUserList(this);
    tcp = TXTCP.create();
    gprs_server = new TGPRS_Server(this, config);
    console_server = new TConsole_Server(this, config);
  }

  @Override
  public void print(final TSensor sensor, final String msg){
    System.out.println((sensor==null ? "" : "s="+sensor.sn+" ")+msg);
  }

  public TUser get_user_by_sn(final int sn, final boolean create){
    return user_list.get_by_sn(sn, create);
  }

  public THub get_hub_by_sn(final int sn, final boolean create){
    return hub_list.get_by_sn(sn, ESensorType.GPRS_MAIN, create);
  }

  public TSensor get_sensor_by_sn(final int sn){
    return sensor_list.get_by_sn(sn);
  }

  public TSensor get_sensor_by_sn(final int sn, final ESensorType type, final boolean create){
    return sensor_list.get_by_sn(sn, type, create);
  }

  public boolean is_gprs_main(final int sn){
    final TSensor s = TService.this.get_sensor_by_sn(sn);
    return s!=null&&s.type==ESensorType.GPRS_MAIN;
  }

  /*
   * cria uma lista secundaria de sensores vinculada à principal
   */
  public TSensorList create_sensor_list(){
    return new TSensorList(sensor_list);
  }

  public void reconfigure(){
    gprs_server.reconfigure_and_listen();
    console_server.reconfigure_and_listen();
    persistent_handler.configure(config.get_db_host(),
                                 Integer.toString(config.get_db_port()),
                                 config.get_db_name(),
                                 config.get_db_user(),
                                 config.get_db_pass());
  }

  public void execute(){
    config.load_from_file();
    reconfigure();
    //
    synchronized(lock){
      while(!finished){
        try{
          lock.wait();
        }catch(final InterruptedException ex){
        }
      }
    }
  }

  public void finish(){
    synchronized(lock){
      finished = true;
      lock.notify();
    }
  }

  public void sensor_changed(final TSensor sensor){
    user_list.sensor_changed(sensor);
  }

}
