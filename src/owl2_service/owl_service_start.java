/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_service;

/**
 *
 */
public class owl_service_start {

  /**
   * @param args the command line arguments
   */
  public static void main(final String[] args){
    A_VERSION.check();
    final TService service = new TService(args.length==0 ? "./owl2-service.txt" : args[0]);
    service.execute();
  }

}
