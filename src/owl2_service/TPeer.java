/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package owl2_service;

import owl2_lib.IPeer;
import owl2_lib.cmd.ICMD_Handler;
import owl2_lib.sensor.TPack;
import x.etc.io.socket.tcp.TXTCPPeerClientForServer;
import x.etc.lib;

/**
 *
 */
public class TPeer extends TXTCPPeerClientForServer implements IPeer {

  final private TService service;
  public byte peer_type;
  public int activity; // controle de ciclos de atividade para desconexão automática
  //
  final private byte[] header;
  private int headerCount;
  private int bufferCount;
  final private TPack pack;
  private ICMD_Handler handler;

  //typedef struct {
  //  uchar start; // 1 'g'
  //  uint len; // 2 do conteudo APOS cmd somente header = 0
  //  ulong sn; // 4
  //  uint section; // 2
  //  /* cmd
  //   * para o gateway:
  //   * 'l' login ->gw
  //   * 'r' registro de sensor ->gw
  //   * '#' peer_done
  //   *
  //   * para o hub
  //   * 'b'
  //   * '*' server_done
  //   */
  //  uchar cmd; // 1
  //  uint n; // tomado do LSB de timer
  //  //
  //  uint checksum_h; // checksum do header sizeof(header)-4
  //  uint checksum_d; // checksum dos dados (len)
  //} TGPRS_HEADER; // 12+4
  final static private int HEADER_LEN = 12+4;

  public TPeer(final TService service_){
    super(service_.tcp);
    service = service_;
    activity = 2; // garante uma checagem de atividade
    header = new byte[HEADER_LEN];
    pack = new TPack(true);
  }

  public void send_ping(){
    handler.send_ping();
  }

  public boolean is_remote_control(){
    return handler!=null&&handler.is_remote_control();
  }

  @Override
  protected void onDataAvailable(final byte[] data){
    int index = 0;
    try{
      while(index<data.length){
        if(headerCount<HEADER_LEN){
          final int needed = HEADER_LEN-headerCount;
          final int available = data.length-index;
          final int until = available<needed ? available : needed;
          for(int i = 0; i<until; i++){
            header[headerCount++] = data[index+i];
          }
          index += until;
          //
          if((header[0]!='g'&&header[0]!='u')||(peer_type!=0&&header[0]!=peer_type)){
            lib.unhandledException("peer", new Exception("header inválido"));
            index = data.length;
            close();
          }else if(headerCount==HEADER_LEN){
            pack.read_header(header);
            int check = lib.checkSum(header, 0, HEADER_LEN-4-1)&0xFFFF;
            if(check==pack.checksum_h){
              bufferCount = 0;
            }else{
              lib.unhandledException("peer", new Exception("checksum_h error"));
              index = data.length;
              close();
            }
          }
        }
        if(headerCount==HEADER_LEN){
          final int needed = pack.len-bufferCount;
          final int available = data.length-index;
          final int until = available<needed ? available : needed;
          for(int i = 0; i<until; i++){
            pack.buffer[bufferCount++] = data[index+i];
          }
          index += until;
          //
          if(bufferCount==pack.len){
            // completou os dados
            if((lib.checkSum(pack.buffer, 0, pack.len-1)&0xFFFF)==pack.checksum_d){
              if(handler==null){
                peer_type = header[0];
                switch(peer_type){
                  case 'g':
                    service.print(null, "obtendo hub "+pack.sn);
                    handler = service.get_hub_by_sn(pack.sn, true);
                    break;

                  case 'u':
                    service.print(null, "obtendo user "+pack.sn);
                    handler = service.get_user_by_sn(pack.sn, true);
                    break;
                }
                if(handler!=null){
                  handler.peer_connected(this);
                }
              }
              if(handler!=null){
                activity = 2;
                pack.read_body_and_execute(handler);
              }
              // proximo pacote
              headerCount = 0;
            }else{
              lib.unhandledException("gprs peer", new Exception("checksum_d error"));
              index = data.length;
              close();
            }
          }
        }

      }
    }catch(final Exception ex){
      if(handler==null){
        lib.unhandledException(ex);
      }else{
        handler.on_exception("peer identification", ex);
      }
      close();
    }

  }

  @Override
  protected void onConnect(){
  }

  @Override
  protected void onDisconnect(){
    if(handler!=null){
      handler.peer_disconnected(this);
    }
  }

}
